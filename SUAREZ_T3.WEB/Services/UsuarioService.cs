﻿using Microsoft.AspNetCore.Http;
using SUAREZ_T3.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SUAREZ_T3.WEB.Services
{
    public interface IUsuarioService
    {
        Usuario GetUser(string username, string password);
        void SetHttpContext(HttpContext httpContext);

    }
    public class UsuarioService : IUsuarioService
    {
        private readonly AppMascotasContext app;
        private HttpContext httpContext;

        public UsuarioService(AppMascotasContext app)
        {
            this.app = app;
        }

        public Usuario GetUser(string username, string password)
        {
            var user = app.Usuarios.Where(o => o.Username == username && o.Password == password).FirstOrDefault();
            return user;
        }

        public void SetHttpContext(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }

    }
}
