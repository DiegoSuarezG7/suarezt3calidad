﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using SUAREZ_T3.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SUAREZ_T3.WEB.Services
{
    public interface IAuthCookieService
    {
        void SetHttpContext(HttpContext httpContext);
        void Login(ClaimsPrincipal principal);
        Usuario LoggedUser();
    }
    public class AuthCookieService : IAuthCookieService
    {
        private HttpContext httpContext;
        private readonly AppMascotasContext app;
        public AuthCookieService(AppMascotasContext app)
        {
            this.app = app;
        }
        public void SetHttpContext(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }
        public void Login(ClaimsPrincipal principal)
        {
            httpContext.SignInAsync(principal);
        }

        public Usuario LoggedUser()
        {
            var claim = httpContext.User.Claims.FirstOrDefault();
            var user = app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }
    }
}
