﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SUAREZ_T3.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SUAREZ_T3.WEB.Services
{
    public interface IHistoriaService
    {
        List<Historia> ListaHistorias();
        SelectList ListaEspecies(); 
        SelectList ListaRazas();
        void GuardarHistoria(Historia historia);
        bool CodigoExiste(Historia historia);
    }
    public class HistoriaService : IHistoriaService
    {
        private readonly AppMascotasContext app;

        public HistoriaService(AppMascotasContext app)
        {
            this.app = app;
        }
        public List<Historia> ListaHistorias()
        {
            return app.Historias.Include(o => o.Especie).Include(o => o.Raza).ToList();
        }

        public SelectList ListaEspecies()
        {
            return new SelectList(app.Especies.Include(o => o.Historias).ToList(), "Id", "NombreEspecie");
        }

        public SelectList ListaRazas()
        {
            return new SelectList(app.Razas.Include(o => o.Historias).ToList(), "Id", "NombreRaza");
        }

        public void GuardarHistoria(Historia historia)
        {
            app.Add(historia);
            app.SaveChanges();
        }

        public bool CodigoExiste(Historia historia)
        {
            var existe = app.Historias.Count(a => a.CodigoRegistro.Equals(historia.CodigoRegistro)) > 0;
            return existe;
        }
    }
}
