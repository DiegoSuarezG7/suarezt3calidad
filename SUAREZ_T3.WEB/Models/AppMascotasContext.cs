﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SUAREZ_T3.WEB.Models
{
    public class AppMascotasContext : DbContext
    {
        public AppMascotasContext()
        {
        }

        public AppMascotasContext(DbContextOptions<AppMascotasContext> options)
        : base(options)
        { }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Historia> Historias { get; set; }
        public DbSet<Especie> Especies { get; set; }
        public DbSet<Raza> Razas { get; set; }


    }
}
