﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SUAREZ_T3.WEB.Models
{
    internal class AdultAgeValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime dt = (DateTime)value;
            if (dt <= DateTime.UtcNow)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(ErrorMessage ?? "La fecha no puede ser posterior a hoy");
        }
    }
}