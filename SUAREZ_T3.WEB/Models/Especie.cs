﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SUAREZ_T3.WEB.Models
{
    public class Especie
    {
        public int Id { get; set; }
        public string NombreEspecie { get; set; }
        public virtual ICollection<Historia> Historias { get; set; }
    }
}
