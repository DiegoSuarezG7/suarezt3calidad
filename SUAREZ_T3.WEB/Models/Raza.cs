﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SUAREZ_T3.WEB.Models
{
    public class Raza
    {
        public int Id { get; set; }
        public string NombreRaza { get; set; }
        public virtual ICollection<Historia> Historias { get; set; }
    }
}
