﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SUAREZ_T3.WEB.Models
{
    public class Historia
    {
        public int Id { get; set; }

        [Display(Name = "Código de Registro")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere el código de registro")]
        public string CodigoRegistro { get; set; }

        [AdultAgeValidation]
        [Display(Name = "Fecha del Registro")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:mm-dd-yyyy}", ApplyFormatInEditMode = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere la fecha")]
        public DateTime Fecha { get; set; }

        [Display(Name = "Nombre de la Mascota")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere el código de registro")]
        public string NombreMascota { get; set; }

        [AdultAgeValidation]
        [Display(Name = "Nacimiento de la mascota")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:mm-dd-yyyy}", ApplyFormatInEditMode = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere la fecha")]
        public DateTime FechaNacimientoMascota { get; set; }

        [Display(Name = "Sexo")]
        public string Sexo { get; set; }

        [Display(Name = "Tamaño")]
        public float Tamanio { get; set; }

        public string DatosParticulares { get; set; }

        [Display(Name = "Nombre del Propietario")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere el nombre del propietario")]
        public string NombrePropietario { get; set; }

        [Display(Name = "Dirección del Propietario")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere la dirección del propietario")]
        public string DireccionPropietario { get; set; }

        [Display(Name = "Teléfono")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere el teléfono")]
        public string Telefono { get; set; }

        [Display(Name = "Correo Eletronico")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Se requiere el Correo")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Especie")]
        public int EspecieId { get; set; }

        [Display(Name = "Raza")]
        public int RazaId { get; set; }

        public Especie Especie { get; set; }

        public Raza Raza { get; set; }

    }
}
