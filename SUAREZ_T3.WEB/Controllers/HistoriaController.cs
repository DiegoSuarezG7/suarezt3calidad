﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SUAREZ_T3.WEB.Models;
using SUAREZ_T3.WEB.Services;

namespace SUAREZ_T3.WEB.Controllers
{
    [Authorize]
    public class HistoriaController : Controller
    {
        private readonly IHistoriaService historiaService;
        public HistoriaController(IHistoriaService historiaService)
        {
            this.historiaService = historiaService;
        }


        [HttpGet]
        public ActionResult Index()
        {

            var historias = historiaService.ListaHistorias();
            return View(historias);
        }

        [HttpGet]
        public ActionResult Crear()
        {
            ViewData["especiesId"] = historiaService.ListaEspecies();
            ViewData["razaId"] = historiaService.ListaRazas();
            return View(new Historia());
        }

        [HttpPost]
        public ActionResult Crear(Historia historia)
        {
            if (!historiaService.CodigoExiste(historia))
            {
                if (!ModelState.IsValid)
                {
                    return View(historia);
                }
                historiaService.GuardarHistoria(historia);
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.error = "Codigo de Registro existe";
                return View("Crear", new Historia());
            }
        }

    }
}
