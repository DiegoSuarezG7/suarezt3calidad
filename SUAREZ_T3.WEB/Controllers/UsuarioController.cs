﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using SUAREZ_T3.WEB.Models;
using SUAREZ_T3.WEB.Services;

namespace SUAREZ_T3.WEB.Controllers
{
    public class UsuarioController : Controller
    {
        private readonly IUsuarioService usuarioService;
        private readonly IAuthCookieService authCookieService;

        public UsuarioController(IUsuarioService usuarioService, IAuthCookieService authCookieService)
        {
            this.usuarioService = usuarioService;
            this.authCookieService = authCookieService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var usuario = usuarioService.GetUser(username, password);
            if (usuario != null)
            {
                var claims = new List<Claim> {
                    new Claim(ClaimTypes.Name, username)
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);


                authCookieService.SetHttpContext(HttpContext);
                authCookieService.Login(claimsPrincipal);

                return RedirectToAction("Index", "Historia");
            }


            ViewBag.Validation = "Usuario y/o contraseña incorrecta";
            return View();
        }


        public ActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}
