﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using SUAREZ_T3.WEB.Controllers;
using SUAREZ_T3.WEB.Models;
using SUAREZ_T3.WEB.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace SUAREZ_T3.TEST.ControllersTests
{
    class UsuarioTest
    {
        [Test]
        public void LoginISnotnull()
        {
            var userMock = new Mock<IUsuarioService>();
            userMock.Setup(o => o.GetUser("admin", "admin")).Returns(new Usuario { });

            var authMock = new Mock<IAuthCookieService>();

            var controller = new UsuarioController(userMock.Object, authMock.Object);
            var result = controller.Login("admin", "admin");

            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }

        [Test]
        public void logoutIsNull()
        {
            Usuario us = null;

            var mock = new Mock<IUsuarioService>();
            mock.Setup(a => a.GetUser("admin", "admin")).Returns(us);
            var controller = new UsuarioController(mock.Object, null);
            var view = (ViewResult)controller.Login("admin", "admin");

            Assert.IsNull(view.Model);

        }
    }
}
