﻿
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using SUAREZ_T3.WEB.Controllers;
using SUAREZ_T3.WEB.Models;
using SUAREZ_T3.WEB.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace SUAREZ_T3.TEST.ControllersTests
{
    class HistoriaTest
    {
        [Test]
        public void IndexDebeRetornarUnaVista()
        {
            var historias = new Mock<IHistoriaService>();
            historias.Setup(o => o.ListaHistorias()).Returns(new List<Historia>());

            var controller = new HistoriaController(historias.Object);
            var index = controller.Index();
            Assert.IsInstanceOf<ViewResult>(index);
        }

        [Test]
        public void CrearHistora()
        {

            var historias = new Mock<IHistoriaService>();
            historias.Setup(o => o.GuardarHistoria(new Historia
            {
                CodigoRegistro ="1234",
                Fecha = new DateTime(2019, 3, 15),
                NombreMascota = "Lycan",
                FechaNacimientoMascota = new DateTime(2020, 11, 4),
                Sexo = "Masculino",
                Tamanio = 0.4f,
                DatosParticulares = "Esta en tratamiento",
                NombrePropietario = "Diego Suarez",
                DireccionPropietario = "Jr Las frores 123",
                Telefono = "123456789",
                Email = "diegosuarez@gmail.com",
                EspecieId =1,
                RazaId =1,
            }));

            var controller = new HistoriaController(historias.Object);
            var index = controller.Index();
            Assert.IsInstanceOf<ViewResult>(index);
        }
    }
}
